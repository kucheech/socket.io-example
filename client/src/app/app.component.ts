import { Component } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SocketService } from './socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';

  messages: Subject<any>;

  constructor(private socketService: SocketService) {
    this.messages = <Subject<any>>socketService
      .connect()
      .pipe(map((response: any): any => {
        return response;
      }));

    this.messages.subscribe(msg => {
      console.log(msg);
    });

    this.sendMsg('test');
  }

  sendMsg(msg) {
    this.messages.next(msg);
  }
}
