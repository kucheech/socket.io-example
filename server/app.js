
const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const CLIENT = '../client/dist/client';
const CLIENT_FOLDER = path.join(__dirname, CLIENT);
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

io.on('connection', (socket) => {
  console.log('new connection');
  socket.on('client', (data) => {
    console.log(data);
  });
  socket.emit('server', 'hi');
});

// catch all
app.use((req, res) => {
  console.info('404 Method %s, Resource %s', req.method, req.originalUrl);
  res.status(404).type('text/html').send('<h1>404 Resource not found</h1>');
});

const PORT = process.env.NODE_PORT || 3000;
server.listen(PORT, () => {
  console.log(`App started at port ${PORT}`);
});
